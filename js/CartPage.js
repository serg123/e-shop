import CONFIG from '../config.js'
import cart from './cart.js'

export default {
    data() {
        return {
            items: [],
        };
    },
    template: `
<div class="container">
<nav class="row m-3">
  <h3>Shopping cart</h3>
  <button class="btn btn-primary text-white font-weight-bold align-self-center ml-auto" @click="back">Back</button>
</nav>
<div class="row">
    <div class="col-12">
        <table class="table table-hover">
            <thead><tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col" class="d-none d-sm-table-cell">Country</th>
                <th scope="col" class="d-none d-sm-table-cell">Price</th>
                <th scope="col" class="d-none d-sm-table-cell"></th>
            </tr></thead>
            <tbody>
            <tr v-for="item in items" @click="navigateItem(item)" style="cursor:pointer">
                <th scope="row">{{item.id}}</th>
                <td>{{item.name}}</td>
                <td class="d-none d-sm-table-cell">{{item.store}}</td>
                <td class="d-none d-sm-table-cell">{{item.price+' '+item.currency}}</td>
                <td class="d-none d-sm-table-cell"><a class="text-danger" @click="remove(item);$event.stopPropagation()">Remove</a></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row m-2">
    <h5>Total: {{totalItems}} item(s), {{totalPrice}} EUR</h5>
</div>
</div>
    `,
    created() {  //fetch only once and attach to the window, use the attached copy afterwards
        let request = window.data ?
            Promise.resolve(window.data) :
            fetch(`${CONFIG.API_URL}?AUTH=${CONFIG.API_KEY}`).then((response) => {
                return response.json();
            });
        request.then((data) => {
            this.items = cart.filter(data);
            window.data = data;
        })
    },
    methods: {
        navigateItem(item) {
            this.$router.push({name: 'details', params: {id: item.id, backPage: 'cart'}})
        },
        back() {
            this.$router.push({name: 'index'});
        },
        remove(item) {
            cart.remove(item);
            this.items = cart.filter(data);
        }
    },
    computed: {
        totalItems() {
            return this.items.length;
        },
        totalPrice() {
            let price = 0;
            for (let item of this.items)
                price += item.price;
            return price
        }
    }
}
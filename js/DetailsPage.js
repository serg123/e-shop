import CONFIG from '../config.js'
import cart from './cart.js'

export default {
    data() {
        return {
            item: {},
            instock: false,
            cartMessage: false,
        };
    },
    props: ['id', 'backPage'],
    template: `
<div class="container">
<nav class="row m-3">
  <h3>{{item.name}}</h3>
</nav>
<div class="row">
    <div class="col-sm-7">
        <table class="table col-sm-7">
            <tr><th>Id:</th><td>{{item.id}}</td></tr>
            <tr><th>Code:</th><td>{{item.productcode}}</td></tr>
            <tr><th>Description:</th><td>{{item.description}}</td></tr>
            <tr><th>Department:</th><td>{{item.department}}</td></tr>
            <tr><th>Price:</th><td>{{item.price+' '+item.currency}}</td></tr>
            <tr><th>Country:</th><td>{{item.store}}</td></tr>
            <tr><th>Available:</th><td>{{instock}}</td></tr>
        </table>
    </div>
    <div class="col-sm-5">
        <img class="d-block d-sm-inline m-auto":src="item.image"></img>
    </div>
    <div class="col-sm-7">
        <button class="btn btn-primary text-white font-weight-bold m-3" @click="back">Back</button>
        <button class="btn btn-success text-white font-weight-bold m-3" @click="addToCart" v-if="!cartMessage">Add to cart</button>
        <button class="btn disabled font-weight-bold m-3" v-if="cartMessage">{{cartMessage}}</button>
    </div>
</div>
</div>
    `,
    created() {  //fetch only once and attach to the window, use the attached copy afterwards
        let request = window.data ?
            Promise.resolve(window.data) :
            fetch(`${CONFIG.API_URL}?AUTH=${CONFIG.API_KEY}`).then((response) => {
                return response.json();
            });
        request.then((data) => {
            for (let d of data) {
                if (d.id == this.id) {
                    this.item = d;
                    this.instock = (d.instock ? CONFIG.TRUE : CONFIG.FALSE);
                    if (!d.instock)
                        this.cartMessage = CONFIG.OUT_OF_STOCK;
                    else if (cart.contains(d))
                        this.cartMessage = CONFIG.IN_CART;
                }
            }
            window.data = data;
        })
    },
    methods: {
        back() {
            if (this.backPage)
                this.$router.push({name: this.backPage});
            else
                this.$router.push({name: 'index'});
        },
        addToCart() {
            cart.add(this.item);
            this.cartMessage = CONFIG.IN_CART;
        },
    }
}
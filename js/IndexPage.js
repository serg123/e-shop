import CONFIG from '../config.js'

export default {
    data() {
        return {
            items: [],
            allItems: [],
            filterCountries: [],
            filterAvailability: [],
            filterCountry: CONFIG.UNSPECIFIED,
            filterAvailable: CONFIG.UNSPECIFIED
        };
    },
    template: `
<div class="container">
<nav class="row m-3">
  <h3>Product list</h3>
  <button class="btn btn-warning text-white font-weight-bold align-self-center ml-auto" @click="navigateCart">Cart</button>
</nav>
<div class="row mt-4 mb-4">
    <div class="col-12">
        <form class="form-inline">
            <label for="country">Country:</label>
            <select class="form-control ml-sm-2 mr-sm-4" id="country" @change="filter" v-model="filterCountry">
                <option v-for="country in filterCountries" :value="country">{{country}}</option>
            </select>
            <label for="available">Available:</label>
            <select class="form-control ml-sm-2" id="available" @change="filter" v-model="filterAvailable">
                <option v-for="country in filterAvailability" :value="country">{{country}}</option>
            </select>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-hover">
            <thead><tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col" class="d-none d-sm-table-cell">Country</th>
                <th scope="col" class="d-none d-sm-table-cell">Price</th>
                <th scope="col" class="d-none d-sm-table-cell">Available</th>
            </tr></thead>
            <tbody>
            <tr v-for="item in items" @click="navigateItem(item)" style="cursor:pointer">
                <th scope="row">{{item.id}}</th>
                <td>{{item.name}}</td>
                <td class="d-none d-sm-table-cell">{{item.store}}</td>
                <td class="d-none d-sm-table-cell">{{item.price+' '+item.currency}}</td>
                <td class="d-none d-sm-table-cell">{{item.instock?'yes':'no'}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
    `,
    created() {  //fetch only once and attach to the window, use the attached copy afterwards
        let request = window.data ?
            Promise.resolve(window.data) :
            fetch(`${CONFIG.API_URL}?AUTH=${CONFIG.API_KEY}`).then((response) => {
                return response.json();
            });
        request.then((data) => {
            let countries = new Set();
            for (let d of data)
                countries.add(d.store);
            this.filterCountries = [CONFIG.UNSPECIFIED, ...countries];
            this.filterAvailability = [CONFIG.UNSPECIFIED, CONFIG.TRUE, CONFIG.FALSE];
            this.items = data;
            this.allItems = data;
            window.data = data;
        })
    },
    methods: {
        filter() {
            let items = [];
            for (let item of this.allItems) {
                if (this.filterCountry != CONFIG.UNSPECIFIED)
                    if (this.filterCountry != item.store)
                        continue;
                if (this.filterAvailable != CONFIG.UNSPECIFIED)
                    if (this.filterAvailable != (item.instock ? CONFIG.TRUE : CONFIG.FALSE))
                        continue;
                items.push(item);
            }
            this.items = items;
        },
        navigateItem(item) {
            this.$router.push({name: 'details', params: {id: item.id}})
        },
        navigateCart() {
            this.$router.push({name: 'cart'})
        },
    }
}
import CONFIG from '../config.js'

// uses local storage to store IDs of items in the cart
class Cart {
    constructor() {
        let json = localStorage.getItem(CONFIG.STORAGE_KEY);
        if (json)
            this.itemIDs = JSON.parse(json);
        else
            this.itemIDs = [];
    }

    sync() {
        let json = JSON.stringify(this.itemIDs);
        localStorage.setItem(CONFIG.STORAGE_KEY, json)
    }

    add(item) {
        if (item.id) {
            this.itemIDs.push(item.id);
            this.sync();
        }
    }

    contains(item) {
        return this.itemIDs.indexOf(item.id) >= 0;
    }

    remove(item) {
        let index = this.itemIDs.indexOf(item.id);
        if (index >= 0) {
            this.itemIDs.splice(index, 1);
            this.sync();
        }
    }

    filter(items) {
        let filtered = [];
        for (let item of items)
            if (this.contains(item))
                filtered.push(item);
        return filtered;
    }
}

export default new Cart();
import IndexPage from './IndexPage.js';
import DetailsPage from './DetailsPage.js';
import CartPage from './CartPage.js';

const router = new VueRouter({
    routes: [
        {path: '/', name: 'index', component: IndexPage},
        {path: '/details/:id', name: 'details', component: DetailsPage, props: true},
        {path: '/cart', name: 'cart', component: CartPage},
    ]
});
window.app = new Vue({
    router: router,
    el: '#app',
    template: '<router-view></router-view>',
    created() {
        this.$router.push(document.location.href.split('#')[1] || '/');
    }
})
export default {
    API_URL: 'https://erply-challenge.herokuapp.com/list',
    API_KEY: 'fae7b9f6-6363-45a1-a9c9-3def2dae206d',
    UNSPECIFIED: 'unspecified',
    TRUE: 'yes',
    FALSE: 'no',
    STORAGE_KEY: 'cart',
    IN_CART: 'Already in cart',
    OUT_OF_STOCK: 'Out of stock'
}
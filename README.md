# Description

### Functionality:
- List all products
- Filtering by country and availability
- Show all available product details
- Add products to cart (if available and was not added before)
- List cart content
- Remove products from cart
- Keeps cart content after a user closes his browser

### How to run:
- Uses ES6 and not supported by IE. Tested in Edge, Chrome, Firefox
- Put files into the web server directory (ES6 modules do not work via file://)
- Navigate to this folder (this should open index.html)
- For tests navigate to the test subfolder (this should open index.html in this subfolder)

### Used libs and frameworks:
- Vue.js
- vue-router
- Bootstrap

import cart from '../js/cart.js'

// some test examples for custom browser test runner
Test({
    setUp() {
        this.item = {id: 1999999 + Math.ceil(Math.random() * 999999)};
        this.itemBad = {};
    },
    testShouldContainAfterAdd() {
        cart.add(this.item);
        this.assert(cart.contains(this.item) === true);
    },
    testShouldNotContainAfterRemove() {
        cart.remove(this.item);
        this.assert(cart.contains(this.item) === false);
    },
    testShouldNotContainInexistentItem() {
        this.assert(cart.contains(this.item) === false);
    },
    testShouldNotContainBadItem() {
        cart.add(this.itemBad);
        this.assert(cart.contains(this.itemBad) === false);
    },
    testShouldNotPassFilterIfNotAdded() {
        let filtered = cart.filter([this.item])
        this.assert(filtered.length == 0);
    },
    testShouldPassFilterIfAdded() {
        cart.add(this.item);
        let filtered = cart.filter([this.item])
        this.assert(filtered.length == 1);
        this.assert(filtered[0] === this.item);
    },
    tearDown() {
        cart.remove(this.item);
    }
}, 'Test cart functionality')